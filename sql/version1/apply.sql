ALTER TABLE group_chat_channel MODIFY group_chat_id INT(11) NOT NULL;
ALTER TABLE group_chat_channel MODIFY channel_id INT(11) NOT NULL;

ALTER TABLE group_chat_user MODIFY group_chat_id INT(11) NOT NULL;
ALTER TABLE group_chat_user MODIFY user_id INT(11) NOT NULL;

ALTER TABLE channel_user MODIFY channel_id INT(11) NOT NULL;
ALTER TABLE channel_user MODIFY user_id INT(11) NOT NULL;

ALTER TABLE channel_channel_message MODIFY channel_id INT(11) NOT NULL;
ALTER TABLE channel_channel_message MODIFY message_id INT(11) NOT NULL;

ALTER TABLE channel_message_photo MODIFY message_id INT(11) NOT NULL;
ALTER TABLE channel_message_photo MODIFY photo_id INT(11) NOT NULL;

ALTER TABLE channel_message_file MODIFY message_id INT(11) NOT NULL;
ALTER TABLE channel_message_file MODIFY file_id INT(11) NOT NULL;
