CREATE TABLE group_chat (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(250),
    owner_id INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    is_deleted BOOLEAN NOT NULL DEFAULT 0
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE group_chat_user (
    group_chat_id INT,
    user_id INT,

    CONSTRAINT GroupChatUser UNIQUE (group_chat_id, user_id)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE channel (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(250),
    owner_id INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    is_deleted BOOLEAN NOT NULL DEFAULT 0
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE group_chat_channel (
    group_chat_id INT,
    channel_id INT,

    CONSTRAINT GroupChatChannel UNIQUE (group_chat_id, channel_id)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE channel_user (
    channel_id INT,
    user_id INT,
    is_unread BOOLEAN DEFAULT true,

    CONSTRAINT ChannelUser UNIQUE (channel_id, user_id)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE channel_message (
    id INT PRIMARY KEY AUTO_INCREMENT,
    content TEXT,
    owner_id INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    is_deleted BOOLEAN NOT NULL DEFAULT 0
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE channel_channel_message (
    channel_id INT,
    message_id INT,

    CONSTRAINT ChannelChannelMessage UNIQUE (channel_id, message_id)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE channel_message_photo (
    message_id INT,
    photo_id INT,

    CONSTRAINT ChannelMessagePhoto UNIQUE (message_id, photo_id)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE channel_message_file (
    message_id INT,
    file_id INT,

    CONSTRAINT ChannelMessageFile UNIQUE (message_id, file_id)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;