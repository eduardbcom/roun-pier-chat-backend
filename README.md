## RounPier Chat Backend

Service that used to serve chat functionality

---

### Production build
```
npm i --production
```

### How to run
```
NODE_PATH=. NODE_ENV=${production|development} node build/src/run
```
