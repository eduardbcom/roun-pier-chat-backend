import { ExtendableError } from '@src/Error/ExtendableError';

class ApplicationError extends ExtendableError {}

export { ApplicationError };
