import { EventEmitter } from 'events';

import { ApplicationError } from '@src/Application/Error';
import { IConfig as ILoggerConfig, Logger } from '@src/Logger/Logger';
import { LoggerWithContext } from '@src/Logger/LoggerWithContext';
import { configValidator } from '@src/Tools';
import { Server, IConfig as IServerConfig } from '@src/Server';
import { ChatsAPIv1 } from '@src/Server/ChatsAPIv1';
import {
    ChatsService,
    ChannelsService,
    MessagesService,
    ConnectionsPoolService,
    IConnectionsPoolServiceConfig,
    ParticipantsSocketsService
} from '@src/Service';

interface IConfig {
    name: string;
    db: IConnectionsPoolServiceConfig;
    server: IServerConfig;
    logger: ILoggerConfig;
}

class Application extends EventEmitter {
    private readonly _applicationLogger: LoggerWithContext;
    private readonly _chatsAPIv1: ChatsAPIv1;
    private readonly _connectionsPoolService: ConnectionsPoolService;
    private readonly _participantsSocketsService: ParticipantsSocketsService;
    private readonly _chatsService: ChatsService;
    private readonly _channelsService: ChannelsService;
    private readonly _messagesService: MessagesService;
    private readonly _server: Server;

    constructor(private readonly config: IConfig) {
        super();

        configValidator(this.config);

        const logger: Logger = new Logger(Object.assign({ appName: this.config.name }, this.config.logger));

        this._applicationLogger = new LoggerWithContext(logger, `[rounpier-chat-backend application ${process.pid}]`);

        this._connectionsPoolService = new ConnectionsPoolService(config.db);
        this._participantsSocketsService = new ParticipantsSocketsService();
        this._chatsService = new ChatsService(
            this._connectionsPoolService,
            this._applicationLogger.create('[chats-service]')
        );
        this._channelsService = new ChannelsService(
            this._connectionsPoolService,
            this._applicationLogger.create('[channels-service]')
        );
        this._messagesService = new MessagesService(this._connectionsPoolService);

        this._chatsAPIv1 = new ChatsAPIv1(
            this._applicationLogger.create('[chats-api-v1]'),
            this._participantsSocketsService,
            this._chatsService,
            this._channelsService,
            this._messagesService
        );

        this._server = new Server(this.config.server, this._applicationLogger.create('[server]'), this._chatsAPIv1);
    }

    public cleanUp(): void {
        this._applicationLogger.close();
    }

    public async start(): Promise<void> {
        const logger = this._applicationLogger.create('(start): ');

        try {
            await this._server.listen();
        } catch (error) {
            logger.error(`Error during starting server while starting application: ${error.message}`, error);

            throw new ApplicationError(
                `Error during starting server while starting application: ${error.message}`,
                error
            );
        }

        logger.debug('Application has been started successfully!');
    }

    public async stop(): Promise<Error[]> {
        const errors: Error[] = [];

        try {
            await this._server.stopListen();
        } catch (error) {
            errors.push(error);
        }

        try {
            await this._connectionsPoolService.destroy();
        } catch (error) {
            errors.push(error);
        }

        // await new Promise(resolve => {
        //     setTimeout(resolve, 60 * 1000);
        // });

        return errors;
    }

    public getLogger(): LoggerWithContext {
        return this._applicationLogger;
    }
}

export { Application };
