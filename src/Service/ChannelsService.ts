import * as assert from 'assert';

import { v1 } from 'rp-chat-types';
import * as mysql from 'mysql2/promise';
import * as _ from 'lodash';

import { ConnectionsPoolService } from './ConnectionsPoolService';
import { LoggerWithContext } from '@src/Logger/LoggerWithContext';

class ChannelsService {
    constructor(
        private readonly connectionsPoolService: ConnectionsPoolService,
        private readonly loggerWithContext: LoggerWithContext
    ) {}

    async new(userId: number, channel: v1.INewChannel): Promise<v1.IChannel> {
        const logger = this.loggerWithContext.create(`[new]({ userId: ${userId}, channel: ${channel} })`);

        logger.debug('Creating new channel...');

        const connection = await this.connectionsPoolService.getConnection();

        logger.debug('Got connection');

        try {
            const channelParticipantsIncludeOwner = _.uniq([userId, ...channel.participants]);

            logger.debug('Channel participants: ', channelParticipantsIncludeOwner);

            const res = await connection.query<mysql.RowDataPacket[]>(
                `
                    INSERT INTO channel (title, owner_id) VALUES ?;
                    SELECT LAST_INSERT_ID() INTO @channel_id;
                    
                    INSERT INTO group_chat_channel (group_chat_id, channel_id) VALUES (?, @channel_id) ON DUPLICATE KEY UPDATE group_chat_id=group_chat_id;

                    ${'INSERT INTO channel_user (channel_id, user_id) VALUES (@channel_id, ?) ON DUPLICATE KEY UPDATE channel_id=channel_id;'.repeat(
                        channelParticipantsIncludeOwner.length
                    )}
                    
                    SELECT
                        c.id as id,
                        UNIX_TIMESTAMP(c.created_at) as createdAt
                    FROM
                        channel as c
                    WHERE
                        c.id = @channel_id
                `,
                [[[channel.title, userId]], channel.chatId, ...channelParticipantsIncludeOwner]
            );

            const [insertedItems] = res;
            const insertedChannels = insertedItems[insertedItems.length - 1];

            const insertedChannel = insertedChannels[0];

            logger.debug('Created channel: ', insertedChannel);

            return {
                id: insertedChannel.id,
                title: channel.title,
                ownerId: userId,
                createdAt: insertedChannel.createdAt,
                chatId: channel.chatId,
                isUnread: true,
                isDeleted: false
            } as v1.IChannel;
        } catch (error) {
            logger.error(`Error on creating new channel: ${error.message}`, error);

            throw error;
        } finally {
            await connection.release();
        }
    }

    async describe(userId: number, channelId: number): Promise<null | v1.IChannelDetails> {
        const logger = this.loggerWithContext.create(`[describe]({ channelId: ${channelId} })`);

        const connection = await this.connectionsPoolService.getConnection();

        logger.debug('Start describing channel..');

        try {
            const [channels] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    c.id,
                    c.title,
                    (SELECT COUNT(*) FROM channel_user as cu WHERE cu.channel_id = c.id) as amountOfMembers,
                    gcc.group_chat_id as chatId,
                    gc.title as chatTitle,
                    c.owner_id as ownerId,
                    c.is_deleted as isDeleted
                FROM
                    channel as c
                INNER JOIN
                    channel_user as cu
                ON
                    cu.channel_id = c.id AND
                    cu.user_id = ?
                INNER JOIN
                    group_chat_channel as gcc
                ON
                    gcc.channel_id = c.id
                INNER JOIN
                    group_chat as gc
                ON
                    gc.id = gcc.group_chat_id
                WHERE
                    c.id = ?
                `,
                [userId, channelId]
            );

            const [channel] = channels;
            if (!channel) {
                return null;
            }

            logger.debug(`Channel ${channelId}: `, channel);

            return {
                id: channel.id,
                title: channel.title,
                amountOfMembers: channel.amountOfMembers,
                chatId: channel.chatId,
                chatTitle: channel.chatTitle,
                ownerId: channel.ownerId,
                isDeleted: channel.isDeleted
            } as v1.IChannelDetails;
        } catch (error) {
            logger.error(`Error on describing channel: ${error.message}`, error);

            throw error;
        } finally {
            await connection.release();
        }
    }

    async members(channelId: number): Promise<v1.IChannelMember[]> {
        const connection = await this.connectionsPoolService.getConnection();

        const logger = this.loggerWithContext.create(`[members]({ channelId: ${channelId} })`);

        logger.debug('Start getting members...');

        try {
            const [studentParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    CONCAT(u.name, ' ', s.lastname) as name,
                    'student' as type,
                    'student' as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    c.owner_id = u.id as is_owner
                FROM
                    channel_user as cu
                INNER JOIN
                    channel as c
                ON
                    c.id = cu.channel_id
                INNER JOIN
                    users as u
                ON
                    cu.user_id = u.id
                INNER JOIN
                    student as s
                ON
                    s.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    cu.channel_id = ?
                `,
                channelId
            );

            const [teacherParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    CONCAT(u.name, ' ', t.lastname) as name,
                    'teacher' as type,
                    t.member as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    c.owner_id = u.id as is_owner
                FROM
                    channel_user as cu
                INNER JOIN
                    channel as c
                ON
                    c.id = cu.channel_id
                INNER JOIN
                    users as u
                ON
                    cu.user_id = u.id
                INNER JOIN
                    teacher as t
                ON
                    t.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    cu.channel_id = ?
                `,
                channelId
            );

            const [orgParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    u.name as name,
                    'org' as type,
                    o.member as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    c.owner_id = u.id as is_owner
                FROM
                    channel_user as cu
                INNER JOIN
                    channel as c
                ON
                    c.id = cu.channel_id
                INNER JOIN
                    users as u
                ON
                    cu.user_id = u.id
                INNER JOIN
                    org as o
                ON
                    o.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    cu.channel_id = ?
                `,
                channelId
            );

            const participants = [...teacherParticipants, ...studentParticipants, ...orgParticipants];

            logger.debug(`Got ${participants.length} members`);

            return participants.map(_ => {
                return {
                    id: _.id,
                    name: _.name,
                    icon: _.icon_id
                        ? this.buildParticipantIconPath({ icon_id: _.icon_id, icon_key: _.icon_key })
                        : undefined,
                    isOwner: _.is_owner,
                    type: _.type,
                    subType: _.sub_type
                } as v1.IChannelMember;
            });
        } finally {
            await connection.release();
        }
    }

    async addMembers(
        _userId: number,
        channelId: number,
        membersToAdd: number[],
        membersToRemove: number[]
    ): Promise<null | {
        channel: v1.IChannel;
        participants: v1.IChannelMember[];
    }> {
        const connection = await this.connectionsPoolService.getConnection();

        try {
            await connection.beginTransaction();

            if (membersToRemove.length) {
                const [{ affectedRows: affectedRows1 }] = await connection.query<mysql.ResultSetHeader>(
                    `DELETE FROM channel_user WHERE (channel_id, user_id) IN (?)`,
                    [membersToRemove.map(_ => [channelId, _])]
                );

                assert(
                    [membersToRemove.length].includes(affectedRows1),
                    `Invalid amount of affectedRows: ${affectedRows1}`
                );
            }

            if (membersToAdd.length) {
                const [{ affectedRows: affectedRows2 }] = await connection.query<mysql.ResultSetHeader>(
                    'INSERT INTO channel_user (channel_id, user_id) VALUES ? ON DUPLICATE KEY UPDATE channel_id=channel_id',
                    [membersToAdd.map(_ => [channelId, _])]
                );

                assert(
                    [membersToAdd.length].includes(affectedRows2),
                    `Invalid amount of affectedRows: ${affectedRows2}`
                );
            }

            const [channels] = await connection.query<mysql.RowDataPacket[]>(
                `
                    SELECT
                        c.id,
                        c.title,
                        c.owner_id,
                        UNIX_TIMESTAMP(c.created_at) as created_at,
                        gcc.group_chat_id as chat_id,
                        cm.id as cmId,
                        cm.content as cmContent,
                        cm.owner_id as cmOwnerId,
                        u.name as cmOwnerName,
                        u.icon_id as cmOwnerIconId,
                        uf.skey as cmOwnerIconKey,
                        UNIX_TIMESTAMP(cm.created_at) as cmCreatedAt,
                        UNIX_TIMESTAMP(cm.updated_at) as cmUpdatedAt
                    FROM
                        channel as c
                    INNER JOIN
                        group_chat_channel as gcc
                    ON
                        gcc.channel_id = c.id
                    LEFT OUTER JOIN
                        channel_message as cm
                    ON
                        cm.id = (
                            SELECT 
                                ccm.message_id
                            FROM
                                channel_channel_message as ccm
                            WHERE
                                ccm.channel_id = c.id
                            ORDER BY
                                ccm.message_id DESC LIMIT 1
                        )
                    LEFT OUTER JOIN
                        users as u
                    ON
                        u.id = cm.owner_id
                    LEFT JOIN
                        user_foto as uf
                    ON
                        uf.id = u.icon_id
                    WHERE
                        c.id = ? AND
                        c.is_deleted = false
                `,
                [channelId]
            );

            console.log('channels: ', channels);

            const [channel] = channels;
            if (!channel) {
                return null;
            }

            const [studentParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    CONCAT(u.name, ' ', s.lastname) as name,
                    'student' as type,
                    'student' as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    c.owner_id = u.id as is_owner
                FROM
                    channel_user as cu
                INNER JOIN
                    channel as c
                ON
                    c.id = cu.channel_id
                INNER JOIN
                    users as u
                ON
                    cu.user_id = u.id
                INNER JOIN
                    student as s
                ON
                    s.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    cu.channel_id = ?
                `,
                channelId
            );

            const [teacherParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    CONCAT(u.name, ' ', t.lastname) as name,
                    'teacher' as type,
                    t.member as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    c.owner_id = u.id as is_owner
                FROM
                    channel_user as cu
                INNER JOIN
                    channel as c
                ON
                    c.id = cu.channel_id
                INNER JOIN
                    users as u
                ON
                    cu.user_id = u.id
                INNER JOIN
                    teacher as t
                ON
                    t.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    cu.channel_id = ?
                `,
                channelId
            );

            const [orgParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    u.name as name,
                    'org' as type,
                    o.member as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    c.owner_id = u.id as is_owner
                FROM
                    channel_user as cu
                INNER JOIN
                    channel as c
                ON
                    c.id = cu.channel_id
                INNER JOIN
                    users as u
                ON
                    cu.user_id = u.id
                INNER JOIN
                    org as o
                ON
                    o.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    cu.channel_id = ?
                `,
                channelId
            );

            const participants = [...teacherParticipants, ...studentParticipants, ...orgParticipants];

            await connection.commit();

            return {
                channel: {
                    id: channel.id,
                    title: channel.title,
                    ownerId: channel.owner_id,
                    createdAt: channel.created_at,
                    chatId: channel.chat_id,
                    isUnread: true,
                    isDeleted: false,
                    message: channel.cmId
                        ? ({
                              id: channel.cmId,
                              content: channel.cmContent,
                              ownerId: channel.cmOwnerId,
                              ownerName: channel.cmOwnerName,
                              ownerImage: channel.cmOwnerIconId
                                  ? this.buildParticipantIconPath({
                                        icon_id: channel.cmOwnerIconId,
                                        icon_key: channel.cmOwnerIconKey
                                    })
                                  : null,
                              createdAt: channel.cmCreatedAt,
                              updatedAt: channel.cmUpdatedAt,
                              chatId: channel.chat_id,
                              channelId: channel.id
                          } as v1.IMessage)
                        : null
                } as v1.IChannel,
                participants: participants.map(_ => {
                    return {
                        id: _.id,
                        name: _.name,
                        icon: _.icon_id
                            ? this.buildParticipantIconPath({ icon_id: _.icon_id, icon_key: _.icon_key })
                            : undefined,
                        isOwner: _.is_owner,
                        type: _.type,
                        subType: _.sub_type
                    } as v1.IChannelMember;
                })
            };
        } catch (error) {
            await connection.rollback();

            throw error;
        } finally {
            await connection.release();
        }
    }

    async allChannelsIds(userId: number): Promise<number[]> {
        const connection = await this.connectionsPoolService.getConnection();

        try {
            const [ids] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    cu.channel_id
                FROM
                    channel_user as cu
                WHERE
                    cu.user_id = ?
                `,
                userId
            );

            return ids.map(_ => _.channel_id);
        } finally {
            await connection.release();
        }
    }

    async getUnreadChannels(userId: number): Promise<v1.IUnreadChannel[]> {
        const logger = this.loggerWithContext.create(`[getUnreadChannels]({ userId: ${userId} })`);

        logger.debug('Start getting unread channels');

        const connection = await this.connectionsPoolService.getConnection();

        try {
            const [res] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    gcc.group_chat_id,
                    cu.channel_id
                FROM
                    channel_user as cu
                INNER JOIN
                    group_chat_channel as gcc
                ON
                    gcc.channel_id = cu.channel_id
                WHERE
                    cu.user_id = ? AND
                    cu.is_unread = true
                `,
                userId
            );

            logger.debug('Got unread channels: ', res);

            return res.map(_ => ({
                chatId: _.group_chat_id,
                channelId: _.channel_id
            }));
        } finally {
            logger.debug('Release connection');

            await connection.release();
        }
    }

    async readChannel(userId: number, channelId: number) {
        const logger = this.loggerWithContext.create(`[readChannel]({ userId: ${userId}, channelId: ${channelId} })`);

        logger.debug('Start reading channel');

        const connection = await this.connectionsPoolService.getConnection();

        try {
            const [{ affectedRows }] = await connection.query<mysql.ResultSetHeader>(
                `
                UPDATE
                    channel_user
                SET
                    is_unread = false
                WHERE
                    user_id = ? AND
                    channel_id = ?
                `,
                [userId, channelId]
            );

            logger.debug('Read: ', affectedRows);
        } finally {
            logger.debug('Release connection');

            await connection.release();
        }
    }

    async deleteChannel(userId: number, channelId: number): Promise<null | v1.IChannel> {
        const logger = this.loggerWithContext.create(`[deleteChannel]({ userId: ${userId}, channelId: ${channelId} })`);

        logger.debug('Start deleting channel');

        const connection = await this.connectionsPoolService.getConnection();

        try {
            const res = await connection.query<mysql.RowDataPacket[]>(
                `
                DELETE FROM
                    channel_user
                WHERE
                    channel_id = ?;
                    
                UPDATE
                    channel
                SET
                    is_deleted = true
                WHERE
                    id = ? AND
                    owner_id = ?;

                SELECT
                    c.id,
                    c.title,
                    c.owner_id,
                    c.is_deleted,
                    c.created_at,
                    gcc.group_chat_id as chat_id
                FROM
                    channel as c
                INNER JOIN
                    group_chat_channel as gcc
                ON
                    gcc.channel_id = c.id
                WHERE
                    c.id = ? AND
                    c.owner_id = ? AND
                    c.is_deleted = true
                `,
                [channelId, channelId, userId, channelId, userId]
            );

            const [deletedItems] = res;
            const deletedChannels = deletedItems[deletedItems.length - 1];
            if (!deletedChannels) {
                return null;
            }

            const deletedChannel = deletedChannels[0];
            if (!deletedChannel) {
                return null;
            }

            logger.debug('Deleted channel: ', deletedChannel);

            return {
                id: deletedChannel.id,
                title: deletedChannel.title,
                ownerId: deletedChannel.owner_id,
                createdAt: deletedChannel.created_at,
                chatId: deletedChannel.chat_id,
                isUnread: true,
                isDeleted: true
            } as v1.IChannel;
        } finally {
            logger.debug('Release connection');

            await connection.release();
        }
    }

    async editChannel(userId: number, channelId: number, title: string): Promise<null | v1.IChannel> {
        const logger = this.loggerWithContext.create(
            `[editChannel]({ userId: ${userId}, channelId: ${channelId}, title: ${title} })`
        );

        logger.debug('Start editing channel');

        const connection = await this.connectionsPoolService.getConnection();

        try {
            const res = await connection.query<mysql.RowDataPacket[]>(
                `
                UPDATE
                    channel
                SET
                    title = ?
                WHERE
                    id = ? AND
                    owner_id = ? AND
                    is_deleted = false;

                SELECT
                    c.id,
                    c.title,
                    c.owner_id,
                    c.is_deleted,
                    c.created_at,
                    gcc.group_chat_id as chat_id
                FROM
                    channel as c
                INNER JOIN
                    group_chat_channel as gcc
                ON
                    gcc.channel_id = c.id
                WHERE
                    c.id = ? AND
                    c.owner_id = ? AND
                    c.is_deleted = false
                `,
                [title, channelId, userId, channelId, userId]
            );

            const [editedtems] = res;
            const editChannels = editedtems[editedtems.length - 1];
            if (!editChannels) {
                return null;
            }

            const editChannel = editChannels[0];
            if (!editChannel) {
                return null;
            }

            logger.debug('Edited channel: ', editChannel);

            return {
                id: editChannel.id,
                title: editChannel.title,
                ownerId: editChannel.owner_id,
                createdAt: editChannel.created_at,
                chatId: editChannel.chat_id,
                isUnread: false,
                isDeleted: false
            } as v1.IChannel;
        } finally {
            logger.debug('Release connection');

            await connection.release();
        }
    }

    async leaveChannel(userId: number, channelId: number): Promise<null | v1.IChannel> {
        const logger = this.loggerWithContext.create(`[leaveChannel]({ userId: ${userId}, channelId: ${channelId} })`);

        logger.debug('Start leaving channel');

        const connection = await this.connectionsPoolService.getConnection();

        try {
            const res = await connection.query<mysql.RowDataPacket[]>(
                `
                DELETE FROM
                    channel_user
                WHERE
                    user_id = ? AND
                    channel_id = ?;

                SELECT
                    c.id,
                    c.title,
                    c.owner_id,
                    c.is_deleted,
                    c.created_at,
                    gcc.group_chat_id as chat_id
                FROM
                    channel as c
                INNER JOIN
                    group_chat_channel as gcc
                ON
                    gcc.channel_id = c.id
                WHERE
                    c.id = ? AND
                    c.is_deleted = false
                `,
                [userId, channelId, channelId]
            );

            const [updatedItems] = res;
            const leftChannels = updatedItems[updatedItems.length - 1];
            if (!leftChannels) {
                return null;
            }

            const leftChannel = leftChannels[0];
            if (!leftChannel) {
                return null;
            }

            logger.debug('Left channel: ', leftChannel);

            return {
                id: leftChannel.id,
                title: leftChannel.title,
                ownerId: leftChannel.owner_id,
                createdAt: leftChannel.created_at,
                chatId: leftChannel.chat_id,
                isUnread: true,
                isDeleted: true
            } as v1.IChannel;
        } finally {
            logger.debug('Release connection');

            await connection.release();
        }
    }

    private buildParticipantIconPath(participant: { icon_id: number; icon_key: string }) {
        return (
            '/files/icon/' +
            Math.ceil(participant.icon_id / 1000) +
            '/' +
            participant.icon_id +
            '_' +
            participant.icon_key +
            'profile.jpg'
        );
    }
}

export { ChannelsService };
