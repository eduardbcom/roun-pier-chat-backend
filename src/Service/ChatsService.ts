import * as assert from 'assert';

import * as mysql from 'mysql2/promise';
import { v1 } from 'rp-chat-types';
import * as _ from 'lodash';

import { LoggerWithContext } from '@src/Logger/LoggerWithContext';

import { ConnectionsPoolService } from './ConnectionsPoolService';

class ChatsService {
    constructor(
        private readonly connectionsPoolService: ConnectionsPoolService,
        private readonly loggerWithContext: LoggerWithContext
    ) { }

    async get(userId: number): Promise<v1.IChat[]> {
        const connection = await this.connectionsPoolService.getConnection();

        try {
            const [_chats] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    gc.id as gId,
                    gc.title as gTitle,
                    UNIX_TIMESTAMP(gc.created_at) as gCreatedAt,
                    gc.owner_id as gOwnerId,
                    c.id as cId,
                    c.title as cTitle,
                    c.owner_id as cOwnerId,
                    UNIX_TIMESTAMP(c.created_at) as cCreatedAt,
                    cu.is_unread as cIsUnread,
                    c.is_deleted as cIsDeleted,
                    cm.id as cmId,
                    cm.content as cmContent,
                    cm.owner_id as cmOwnerId,
                    u.name as cmOwnerName,
                    u.icon_id as cmIconId,
                    uf.skey as cmIconKey,
                    UNIX_TIMESTAMP(cm.created_at) as cmCreatedAt,
                    UNIX_TIMESTAMP(cm.updated_at) as cmUpdatedAt
                FROM
                    group_chat_user AS gcu
                INNER JOIN
                    group_chat AS gc
                ON
                    gc.id = gcu.group_chat_id
                LEFT OUTER JOIN
                    group_chat_channel as gcc
                ON
                    gcc.group_chat_id = gcu.group_chat_id AND
                    gcc.channel_id IN (
                        SELECT
                            channel_id
                        FROM
                            channel_user
                        WHERE
                            user_id = gcu.user_id
                    )
                LEFT JOIN
                    channel_user as cu
                ON
                    cu.channel_id = gcc.channel_id AND
                    cu.user_id = gcu.user_id
                LEFT OUTER JOIN
                    channel as c
                ON
                    c.id = gcc.channel_id
                LEFT JOIN
                    channel_message AS cm
                ON cm.id = (
                    SELECT
                        ccm.message_id
                    FROM
                        channel_channel_message AS ccm
                    WHERE
                        ccm.channel_id = c.id
                    ORDER BY
                        ccm.message_id DESC
                    LIMIT 1
                )
                LEFT OUTER JOIN
                    users AS u
                ON
                    u.id = cm.owner_id
                LEFT JOIN
                    user_foto AS uf
                ON
                    uf.id = u.icon_id
                WHERE
                    gcu.user_id = ?
                ORDER BY
                    gc.created_at DESC,
                    cm.created_at DESC
                `,
                [userId]
            );

            this.loggerWithContext.debug(`Chats for userId ${userId}: `, _chats);

            const chats = _chats.reduce((acc: any, chat: any) => {
                const groupChat: v1.IChat = acc[chat.gId] || {
                    id: chat.gId,
                    title: chat.gTitle,
                    createdAt: chat.gCreatedAt,
                    ownerId: chat.gOwnerId,
                };

                const channels = groupChat.channels || [];

                groupChat.channels = chat.cId ? [...channels, {
                    id: chat.cId,
                    title: chat.cTitle,
                    ownerId: chat.cOwnerId,
                    createdAt: chat.cCreatedAt,
                    chatId: chat.gId,
                    isUnread: !!chat.cIsUnread,
                    isDeleted: !!chat.cIsDeleted,
                    message: chat.cmId ? {
                        id: chat.cmId,
                        content: chat.cmContent,
                        ownerId: chat.cmOwnerId,
                        ownerName: chat.cmOwnerName,
                        ownerImage: chat.cmIconId ? this.buildParticipantIconPath({ icon_id: chat.cmIconId, icon_key: chat.cmIconKey }) : null,
                        createdAt: chat.cmCreatedAt,
                        updatedAt: chat.cmUpdatedAt,
                        chatId: chat.gId,
                        channelId: chat.cId
                    } as v1.IMessage : null
                } as v1.IChannel] : [];

                return { ...acc, [chat.gId]: groupChat };
            }, {});

            return Object.values(chats);
        } finally {
            await connection.release();
        }
    }

    // TODO: speed up
    async new(userId: number, chat: v1.INewChat): Promise<v1.IChat> {
        const participants = _.uniq([userId, ...chat.participants]);

        const connection = await this.connectionsPoolService.getConnection();

        try {
            await connection.beginTransaction();

            const [{ insertId }] = await connection.query<mysql.ResultSetHeader>(
                'INSERT INTO group_chat (title, owner_id) VALUES ?',
                [[[chat.title, userId]]]
            );

            const [{ affectedRows }] = await connection.query<mysql.ResultSetHeader>(
                'INSERT INTO group_chat_user (group_chat_id, user_id) VALUES ? ON DUPLICATE KEY UPDATE group_chat_id=group_chat_id',
                [participants.map(_ => [insertId, _])]
            );

            assert([participants.length].includes(affectedRows), `Invalid amount of affectedRows: ${affectedRows}`);

            const defaultChannelName = 'general';

            const [{ affectedRows: affectedRows2, insertId: channelId }] = await connection.query<mysql.ResultSetHeader>(
                'INSERT INTO channel (title, owner_id) VALUES ?',
                [[[defaultChannelName, userId]]]
            );

            assert([1].includes(affectedRows2), `Invalid amount of affectedRows: ${affectedRows}`);

            const [{ affectedRows: affectedRows3 }] = await connection.query<mysql.ResultSetHeader>(
                'INSERT INTO channel_user (channel_id, user_id) VALUES ? ON DUPLICATE KEY UPDATE channel_id=channel_id',
                [participants.map(_ => [channelId, _])]
            );

            assert([participants.length].includes(affectedRows3), `Invalid amount of affectedRows: ${affectedRows}`);

            const [{ affectedRows: affectedRows4 }] = await connection.query<mysql.ResultSetHeader>(
                'INSERT INTO group_chat_channel (group_chat_id, channel_id) VALUES ? ON DUPLICATE KEY UPDATE group_chat_id=group_chat_id',
                [[[insertId, channelId]]]
            );

            assert([1].includes(affectedRows4), `Invalid amount of affectedRows: ${affectedRows}`);

            await connection.commit();

            const [channels] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    c.id,
                    c.title,
                    c.owner_id,
                    UNIX_TIMESTAMP(c.created_at) as created_at,
                    gc.id as chat_id
                FROM
                    group_chat as gc
                INNER JOIN
                    group_chat_channel as gcc
                ON
                    gc.id = gcc.group_chat_id 
                INNER JOIN
                    channel as c
                ON
                    c.id = gcc.channel_id
                WHERE
                    gc.id = ${insertId}
                `
            );

            return {
                id: insertId,
                title: chat.title,
                ownerId: userId,
                // TODO: fix
                createdAt: channels[0]?.created_at,
                channels: channels.map(_ => ({
                    id: _.id,
                    title: _.title,
                    chatId: _.chat_id,
                    ownerId: _.owner_id,
                    createdAt: _.created_at,
                    isUnread: true
                } as v1.IChannel))
            } as v1.IChat
        } catch (error) {
            await connection.rollback();

            throw error;
        } finally {
            await connection.release();
        }
    }

    async editChat(userId: number, gChatId: number, title: string): Promise<null | v1.IChat> {
        const logger = this.loggerWithContext.create(`[editChat]({ userId: ${userId}, gChatId: ${gChatId}, title: ${title} })`);

        logger.debug('Start editing chat');

        const connection = await this.connectionsPoolService.getConnection();

        try {
            const res = await connection.query<mysql.RowDataPacket[]>(
                `
                UPDATE
                    group_chat
                SET
                    title = ?
                WHERE
                    id = ? AND
                    owner_id = ? AND
                    is_deleted = false;

                SELECT
                    gc.id,
                    gc.title,
                    gc.owner_id,
                    gc.is_deleted,
                    gc.created_at
                FROM
                    group_chat as gc
                WHERE
                    gc.id = ? AND
                    gc.owner_id = ? AND
                    gc.is_deleted = false
                `,
                [title, gChatId, userId, gChatId, userId]
            );

            const [editedtems] = res;
            const editedChats = editedtems[editedtems.length - 1];
            if (!editedChats) {
                return null;
            }

            const editedChat = editedChats[0];
            if (!editedChat) {
                return null;
            }

            logger.debug('Edited chat: ', editedChat);

            return {
                id: editedChat.id,
                title: editedChat.title,
                ownerId: editedChat.owner_id,
                createdAt: editedChat.created_at,
                // TODO: return channels or consider to add new type v1.IEditChatSuccess
                channels: []
            } as v1.IChat;
        } finally {
            logger.debug('Release connection');

            await connection.release();
        }
    }

    async describe(gChatId: number): Promise<null | v1.IChatDetails> {
        const logger = this.loggerWithContext.create(`[describe]({ gChatId: ${gChatId} })`);

        const connection = await this.connectionsPoolService.getConnection();

        logger.debug("Start getting members...");

        try {
            const [chats] = await connection.query<mysql.RowDataPacket[]>(
                `
                    SELECT
                        gc.title
                    FROM
                        group_chat as gc
                    WHERE
                        gc.id = ? AND
                        gc.is_deleted = false
                `,
                [gChatId]
            );

            console.log("chats: ", chats);

            const [chat] = chats;
            if (!chat) {
                return null;
            }

            logger.debug(`Got сhat ${gChatId}: ${JSON.stringify(chat)}`);

            return {
                id: gChatId,
                title: chat.title
            };
        } finally {
            await connection.release();
        }
    }

    async allChatsIds(userId: number): Promise<number[]> {
        const connection = await this.connectionsPoolService.getConnection();

        try {
            const [ids] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    gcu.group_chat_id
                FROM
                    group_chat_user as gcu
                WHERE
                    gcu.user_id = ?
                `,
                userId
            );

            return ids.map(_ => _.group_chat_id);
        } finally {
            await connection.release();
        }
    }

    async members(chatId: number): Promise<v1.IChatMember[]> {
        const connection = await this.connectionsPoolService.getConnection();

        const logger = this.loggerWithContext.create(`[members]({ chatId: ${chatId} })`);

        logger.debug("Start getting members...");

        try {
            const [studentParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    CONCAT(u.name, ' ', s.lastname) as name,
                    'student' as type,
                    'student' as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    gc.owner_id = u.id as is_owner
                FROM
                    group_chat_user as gcu
                INNER JOIN
                    group_chat as gc
                ON
                    gcu.group_chat_id = gc.id
                INNER JOIN
                    users as u
                ON
                    gcu.user_id = u.id
                INNER JOIN
                    student as s
                ON
                    s.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    gcu.group_chat_id = ?
                `,
                chatId
            );

            const [teacherParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    CONCAT(u.name, ' ', t.lastname) as name,
                    'teacher' as type,
                    t.member as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    gc.owner_id = u.id as is_owner
                FROM
                    group_chat_user as gcu
                INNER JOIN
                    group_chat as gc
                ON
                    gcu.group_chat_id = gc.id
                INNER JOIN
                    users as u
                ON
                    gcu.user_id = u.id
                INNER JOIN
                    teacher as t
                ON
                    t.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    gcu.group_chat_id = ?
                `,
                chatId
            );

            const [orgParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    u.name as name,
                    'org' as type,
                    o.member as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    gc.owner_id = u.id as is_owner
                FROM
                    group_chat_user as gcu
                INNER JOIN
                    group_chat as gc
                ON
                    gcu.group_chat_id = gc.id
                INNER JOIN
                    users as u
                ON
                    gcu.user_id = u.id
                INNER JOIN
                    org as o
                ON
                    o.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    gcu.group_chat_id = ?
                `,
                chatId
            );

            const participants = [
                ...teacherParticipants,
                ...studentParticipants,
                ...orgParticipants
            ];

            logger.debug(`Got ${participants.length} members`);

            return participants.map(_ => {
                return {
                    id: _.id,
                    name: _.name,
                    icon: _.icon_id ? this.buildParticipantIconPath({ icon_id: _.icon_id, icon_key: _.icon_key }) : undefined,
                    isOwner: _.is_owner,
                    type: _.type,
                    subType: _.sub_type
                } as v1.IChatMember
            });
        } finally {
            await connection.release();
        }
    }

    async addMembers(_userId: number, gChatId: number, membersToAdd: number[], membersToRemove: number[]): Promise<
        null | {
            chat: v1.IChat, participants: v1.IChatMember[]
        }> {
        const logger = this.loggerWithContext.create(`[addMembers]({ gChatId: ${gChatId}, membersToAdd: ${membersToAdd}, membersToRemove: ${membersToRemove} })`);

        logger.debug("Start editing members...");

        const connection = await this.connectionsPoolService.getConnection();

        try {
            await connection.beginTransaction();

            if (membersToRemove.length) {
                const [{ affectedRows: affectedRows1 }] = await connection.query<mysql.ResultSetHeader>(
                    `DELETE FROM group_chat_user WHERE (group_chat_id, user_id) IN (?)`,
                    [membersToRemove.map(_ => [gChatId, _])]
                );

                assert([membersToRemove.length].includes(affectedRows1), `Invalid amount of affectedRows: ${affectedRows1}`);

                await connection.query<mysql.ResultSetHeader>(
                    `
                    DELETE FROM
                        channel_user
                    WHERE
                        user_id IN (?) AND
                        channel_id IN (
                            SELECT
                                channel_id
                            FROM
                                group_chat_channel
                            WHERE
                                group_chat_id = ?
                        )
                    `,
                    [membersToRemove.map(_ => [_]), gChatId]
                );
            }

            if (membersToAdd.length) {
                const [{ affectedRows: affectedRows2 }] = await connection.query<mysql.ResultSetHeader>(
                    'INSERT INTO group_chat_user (group_chat_id, user_id) VALUES ? ON DUPLICATE KEY UPDATE group_chat_id=group_chat_id',
                    [membersToAdd.map(_ => [gChatId, _])]
                );

                assert([membersToAdd.length].includes(affectedRows2), `Invalid amount of affectedRows: ${affectedRows2}`);
            }

            const [chats] = await connection.query<mysql.RowDataPacket[]>(
                `
                    SELECT
                        gc.id,
                        gc.title,
                        gc.owner_id,
                        UNIX_TIMESTAMP(gc.created_at) as created_at
                    FROM
                        group_chat as gc
                    WHERE
                        gc.id = ? AND
                        gc.is_deleted = false
                `,
                [gChatId]
            );

            const [chat] = chats;
            if (!chat) {
                return null;
            }

            const [studentParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    CONCAT(u.name, ' ', s.lastname) as name,
                    'student' as type,
                    'student' as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    gc.owner_id = u.id as is_owner
                FROM
                    group_chat_user as gcu
                INNER JOIN
                    group_chat as gc
                ON
                    gcu.group_chat_id = gc.id
                INNER JOIN
                    users as u
                ON
                    gcu.user_id = u.id
                INNER JOIN
                    student as s
                ON
                    s.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    gcu.group_chat_id = ?
                `,
                gChatId
            );

            const [teacherParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    CONCAT(u.name, ' ', t.lastname) as name,
                    'teacher' as type,
                    t.member as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    gc.owner_id = u.id as is_owner
                FROM
                    group_chat_user as gcu
                INNER JOIN
                    group_chat as gc
                ON
                    gcu.group_chat_id = gc.id
                INNER JOIN
                    users as u
                ON
                    gcu.user_id = u.id
                INNER JOIN
                    teacher as t
                ON
                    t.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    gcu.group_chat_id = ?
                `,
                gChatId
            );

            const [orgParticipants] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    u.id,
                    u.name as name,
                    'org' as type,
                    o.member as sub_type,
                    u.icon_id,
                    uf.skey as icon_key,
                    gc.owner_id = u.id as is_owner
                FROM
                    group_chat_user as gcu
                INNER JOIN
                    group_chat as gc
                ON
                    gcu.group_chat_id = gc.id
                INNER JOIN
                    users as u
                ON
                    gcu.user_id = u.id
                INNER JOIN
                    org as o
                ON
                    o.id = u.id
                LEFT JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                WHERE
                    gcu.group_chat_id = ?
                `,
                gChatId
            );

            const participants = [
                ...teacherParticipants,
                ...studentParticipants,
                ...orgParticipants
            ];

            await connection.commit();

            logger.debug(`Edited members for ${gChatId}`);

            return {
                chat: {
                    id: chat.id,
                    title: chat.title,
                    ownerId: chat.owner_id,
                    createdAt: chat.created_at,
                    channels: []
                } as v1.IChat,
                participants: participants.map(_ => {
                    return {
                        id: _.id,
                        name: _.name,
                        icon: _.icon_id ? this.buildParticipantIconPath({ icon_id: _.icon_id, icon_key: _.icon_key }) : undefined,
                        isOwner: _.is_owner,
                        type: _.type,
                        subType: _.sub_type
                    } as v1.IChannelMember
                })
            };
        } catch (error) {
            await connection.rollback();

            throw error;
        } finally {
            await connection.release();
        }
    }

    private buildParticipantIconPath(participant: { icon_id: number, icon_key: string }) {
        return '/files/icon/' + Math.ceil(participant.icon_id / 1000) + '/' + participant.icon_id + '_' + participant.icon_key + 'profile.jpg'
    }
}

export { ChatsService };
