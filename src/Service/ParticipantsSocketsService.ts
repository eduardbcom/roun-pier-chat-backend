import { Socket } from 'socket.io';

class ParticipantsSocketsService {
    private _sockets = new Map<number, Socket[]>();

    set(userId: number, socket: Socket) {
        const sockets = this._sockets.get(userId);
        if (!sockets) {
            return this._sockets.set(userId, [socket]);
        }

        return this._sockets.set(userId, [...sockets, socket]);
    }

    delete(userId: number, socket: Socket) {
        const sockets = this._sockets.get(userId) || [];
        const filteredSockets = sockets.filter(_ => _.id !== socket.id);

        this._sockets.set(userId, filteredSockets);
    }

    get(userId: number) {
        return this._sockets.get(userId) || [];
    }
}

export { ParticipantsSocketsService };
