import * as mysql from 'mysql2/promise';

export interface IConfig {
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
}

export class ConnectionsPoolService {
    private _pool: mysql.Pool;

    constructor(
        private readonly _config: IConfig
    ) {
        this._pool = mysql.createPool({
            host: this._config.host,
            port: this._config.port,
            user: this._config.user,
            password: this._config.password,
            database: this._config.database,
            multipleStatements: true
        });
    }

    getConnection() {
        return this._pool.getConnection();
    }

    async destroy() {
        return this._pool.end();
    }
}
