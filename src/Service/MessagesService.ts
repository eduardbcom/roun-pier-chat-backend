import * as assert from 'assert';

import * as mysql from 'mysql2/promise';
import { v1 } from 'rp-chat-types';
import { INewMessage } from 'rp-chat-types/dist/v1';

import { ConnectionsPoolService } from './ConnectionsPoolService';

class MessagesService {
    constructor(private readonly connectionsPoolService: ConnectionsPoolService) {}

    async get(userId: number, channelId: number, page: number): Promise<{ messages: v1.IMessage[]; page: number }> {
        const connection = await this.connectionsPoolService.getConnection();

        const pageSize = 50;

        try {
            // TODO: first last name
            const [rawMessages] = await connection.query<mysql.RowDataPacket[]>(
                `
                SELECT
                    cm.id,
                    cm.content,
                    cm.owner_id,
                    u.name as owner_name,
                    uf.id as icon_id,
                    uf.skey as icon_key,
                    UNIX_TIMESTAMP(cm.created_at) as created_at,
                    UNIX_TIMESTAMP(cm.updated_at) as updated_at,
                    gcc.group_chat_id as chatId,
                    ccm.channel_id as channelId,
                    uf2.id as photoId,
                    uf2.mime as photoMime,
                    uf2.skey as photoKey,
                    uf2.width as photoWidth,
                    uf2.height as photoHeight,
                    ufiles.id as fileId,
                    ufiles.name as fileName,
                    ufiles.mime as fileMime,
                    ufiles.skey as fileKey,
                    ufiles.size as fileSize
                FROM
                    channel_message as cm
                INNER JOIN
                    (SELECT * FROM channel_channel_message WHERE channel_id = ? ORDER BY message_id DESC LIMIT ? OFFSET ?) as ccm
                ON
                    cm.id = ccm.message_id AND
                    cm.is_deleted = false
                INNER JOIN
                    channel as c
                ON
                    c.id = ccm.channel_id AND
                    c.is_deleted = false
                INNER JOIN
                    channel_user as cu
                ON
                    cu.channel_id = c.id AND
                    cu.user_id = ?
                INNER JOIN
                    group_chat_channel as gcc
                ON
                    gcc.channel_id = ccm.channel_id
                INNER JOIN
                    users as u
                ON
                    u.id = cm.owner_id
                LEFT OUTER JOIN
                    user_foto as uf
                ON
                    uf.id = u.icon_id
                LEFT OUTER JOIN
                    channel_message_photo as cmp
                ON
                    cmp.message_id = ccm.message_id
                LEFT OUTER JOIN
                    user_foto as uf2
                ON
                    uf2.id = cmp.photo_id
                LEFT OUTER JOIN
                    channel_message_file as cmf
                ON
                    cmf.message_id = ccm.message_id
                LEFT OUTER JOIN
                    user_files as ufiles
                ON
                    ufiles.id = cmf.file_id
                `,
                [channelId, pageSize, page * pageSize, userId]
            );

            console.log('rawMessages: ', rawMessages.length);

            const normalizedMessages = rawMessages.reduce((acc: any, message: mysql.RowDataPacket) => {
                console.log('messageid: ', message.id);

                const m =
                    acc[message.id] ||
                    ({
                        id: message.id,
                        content: message.content,
                        ownerId: message.owner_id,
                        ownerName: message.owner_name,
                        ownerImage: message.icon_id
                            ? this.buildParticipantIconPath({ icon_id: message.icon_id, icon_key: message.icon_key })
                            : null,
                        createdAt: message.created_at,
                        updatedAt: message.updated_at,
                        chatId: message.chatId,
                        channelId: message.channelId
                    } as v1.IMessage);

                m.photos = m.photos || [];
                m.files = m.files || [];

                if (message.photoId) {
                    m.photos = [
                        ...m.photos,
                        {
                            id: message.photoId,
                            url: this.buildImageURL(message.photoId, message.photoMime, message.photoKey),
                            width: message.photoWidth,
                            height: message.photoHeight
                        }
                    ];
                }

                if (message.fileId) {
                    m.files = [
                        ...m.files,
                        {
                            id: message.fileId,
                            url: this.buildFileURL(message.fileId, message.fileMime, message.fileKey),
                            name: message.fileName,
                            size: message.fileSize
                        }
                    ];
                }

                return {
                    ...acc,
                    [message.id]: m
                };
            }, {});

            const messages = Object.values<v1.IMessage>(normalizedMessages);

            console.log('messages: ', messages, messages.length);

            return {
                messages,
                page: messages.length === pageSize ? page + 1 : page
            };
        } finally {
            await connection.release();
        }
    }

    async new(userId: number, message: INewMessage): Promise<v1.IMessage> {
        const connection = await this.connectionsPoolService.getConnection();

        try {
            const res = await connection.query<mysql.RowDataPacket[]>(
                `
                    INSERT INTO channel_message (content, owner_id) VALUES ?;
                    SELECT LAST_INSERT_ID() INTO @message_id;
                    
                    ${'INSERT INTO channel_message_photo (message_id, photo_id) VALUES (@message_id, ?) ON DUPLICATE KEY UPDATE message_id=message_id;'.repeat(
                        message.photos_ids.length
                    )}
                    ${'INSERT INTO channel_message_file (message_id, file_id) VALUES (@message_id, ?) ON DUPLICATE KEY UPDATE message_id=message_id;'.repeat(
                        message.files_ids.length
                    )}

                    UPDATE channel_user SET is_unread = true WHERE channel_id = ?;

                    INSERT INTO channel_channel_message (channel_id, message_id) VALUES (?, @message_id) ON DUPLICATE KEY UPDATE channel_id=channel_id;
                    SELECT LAST_INSERT_ID() INTO @channel_channel_message_id;
                    
                    SELECT
                        cm.id,
                        cm.content,
                        cm.owner_id,
                        u.name as owner_name,
                        uf.id as icon_id,
                        uf.skey as icon_key,
                        UNIX_TIMESTAMP(cm.created_at) as created_at,
                        UNIX_TIMESTAMP(cm.updated_at) as updated_at,
                        gcc.group_chat_id as chatId,
                        ccm.channel_id as channelId,
                        uf2.id as photoId,
                        uf2.mime as photoMime,
                        uf2.skey as photoKey,
                        uf2.width as photoWidth,
                        uf2.height as photoHeight,
                        ufiles.id as fileId,
                        ufiles.name as fileName,
                        ufiles.mime as fileMime,
                        ufiles.skey as fileKey,
                        ufiles.size as fileSize
                    FROM
                        channel_message as cm
                    INNER JOIN
                        channel_channel_message as ccm
                    ON
                        cm.id = ccm.message_id
                    INNER JOIN
                        group_chat_channel as gcc
                    ON
                        gcc.channel_id = ccm.channel_id
                    INNER JOIN
                        users as u
                    ON
                        u.id = cm.owner_id
                    LEFT OUTER JOIN
                        user_foto as uf
                    ON
                        uf.id = u.icon_id
                    LEFT OUTER JOIN
                        channel_message_photo as cmp
                    ON
                        cmp.message_id = ccm.message_id
                    LEFT OUTER JOIN
                        user_foto as uf2
                    ON
                        uf2.id = cmp.photo_id
                    LEFT OUTER JOIN
                        channel_message_file as cmf
                    ON
                        cmf.message_id = ccm.message_id
                    LEFT OUTER JOIN
                        user_files as ufiles
                    ON
                        ufiles.id = cmf.file_id
                    WHERE
                        cm.id = @channel_channel_message_id
                `,
                [
                    [[message.content, userId]],
                    ...message.photos_ids,
                    ...message.files_ids,
                    message.channelId,
                    message.channelId
                ]
            );

            const [insertedItems] = res;
            const insertedMessages = insertedItems[insertedItems.length - 1];

            const insertedMessage = insertedMessages[0];

            console.log('inserted Message: ', insertedMessages, insertedMessage);

            const normalizedMessages = [insertedMessage].reduce((acc: any, message: mysql.RowDataPacket) => {
                const m =
                    acc[message.id] ||
                    ({
                        id: message.id,
                        content: message.content,
                        ownerId: message.owner_id,
                        ownerName: message.owner_name,
                        ownerImage: message.icon_id
                            ? this.buildParticipantIconPath({ icon_id: message.icon_id, icon_key: message.icon_key })
                            : null,
                        createdAt: message.created_at,
                        updatedAt: message.updated_at,
                        chatId: message.chatId,
                        channelId: message.channelId
                    } as v1.IMessage);

                m.photos = m.photos || [];
                m.files = m.files || [];

                if (message.photoId) {
                    m.photos = [
                        ...m.photos,
                        {
                            id: message.photoId,
                            url: this.buildImageURL(message.photoId, message.photoMime, message.photoKey),
                            width: message.photoWidth,
                            height: message.photoHeight
                        }
                    ];
                }

                if (message.fileId) {
                    m.files = [
                        ...m.files,
                        {
                            id: message.fileId,
                            url: this.buildFileURL(message.fileId, message.fileMime, message.fileKey),
                            name: message.fileName,
                            size: message.fileSize
                        }
                    ];
                }

                return {
                    ...acc,
                    [message.id]: m
                };
            }, {});

            const [newMessage] = Object.values<v1.IMessage>(normalizedMessages);

            return newMessage;
        } finally {
            await connection.release();
        }
    }

    private buildImageURL(id: number, mime: string, key: string) {
        const destinationPath = '/files/img/' + Math.ceil(id / 1000) + '/';

        return destinationPath + id + '_' + key + '.' + this.typeForMime(mime);
    }

    private buildFileURL(id: number, mime: string, key: string) {
        const destinationPath = '/files/file/' + Math.ceil(id / 1000) + '/';

        return destinationPath + id + '_' + key + '.' + this.typeForMime(mime);
    }

    private buildParticipantIconPath(participant: { icon_id: number; icon_key: string }) {
        return (
            '/files/icon/' +
            Math.ceil(participant.icon_id / 1000) +
            '/' +
            participant.icon_id +
            '_' +
            participant.icon_key +
            'profile.jpg'
        );
    }

    // TODO: use library
    private typeForMime(mime: string) {
        switch (mime) {
            case 'image/jpeg':
                return 'jpg';
            case 'image/png':
                return 'png';
            case 'image/gif':
                return 'gif';
            case 'application/pdf':
                return 'pdf';
            default:
                return '';
        }
    }
}

export { MessagesService };
