import * as assert from 'assert';
import * as http from 'http';
import * as https from 'https';
import * as fs from 'fs';
import * as events from 'events';

import * as io from 'socket.io';
import * as express from 'express';

import { ChatsAPIv1 } from '@src/Server/ChatsAPIv1';
import { LoggerWithContext } from '@src/Logger/LoggerWithContext';
import { ParticipantsSocketsService } from '@src/Service';

export interface IConfig {
    port: number;
    options?: {
        https?: {
            https_key: string;
            https_cert: string;
        }
    }
}

class Server extends events.EventEmitter {
    private readonly app: express.Express;
    private readonly server: http.Server | https.Server;
    private readonly socketServer: io.Server;
    private readonly _openSockets: Set<io.Socket> = new Set();

    constructor(
        private readonly config: IConfig,
        private readonly logger: LoggerWithContext,
        private readonly chatsAPIv1: ChatsAPIv1
    ) {
        super();

        this.app = express();

        this.server = this.createServer(this.app);
        this.socketServer = new io.Server(this.server as http.Server, {
            cors: {
                origin: "*"
            }
        });

        this.socketServer.of('chats/v1').on('connection', async (socket: io.Socket) => {
            const logger = this.logger.create(`[chats/v1]({ id: ${socket.id} })`);
            
            this._openSockets.add(socket);

            try {
                await this.chatsAPIv1.listen(socket);
            } catch (error) {
                logger.error(`Error on listening socket: ${error.message}`, error);

                socket.disconnect();
            }

            socket.on('disconnect', _ => {
                this.chatsAPIv1.stopListen(socket);

                this._openSockets.delete(socket);
            });
        });
    }

    async listen() {
        return new Promise<void>((resolve, reject) => {
            this.server.on('error', reject);

            this.server.listen(this.config.port, () => {
                this.server.removeAllListeners('error');
                this.server.on('error', this.onError);

                resolve();
            });
        });
    }

    async stopListen() {
        this.socketServer.removeAllListeners();
        this.socketServer.of('chats/v1').removeAllListeners();

        // TODO: will not close until active socket connection
        return new Promise<void>((resolve, reject) => {
            this.server.close((error?: Error) => {
                if (error) {
                    return reject(error);
                }

                return resolve();
            });

            this._openSockets.forEach(_ => {
                _.removeAllListeners();
                _.disconnect(true);
            });
            this._openSockets.clear();
        });
    }

    private createServer(app: express.Express) {
        if (this.config?.options?.https) {
            assert(this.config.options.https.https_key, 'Invalid https_key');
            assert(this.config.options.https.https_cert, 'Invalid https_cert');

            return https.createServer({
                key: fs.readFileSync(this.config.options.https.https_key),
                cert: fs.readFileSync(this.config.options.https.https_cert)
            }, app);
        }

        return http.createServer(app);
    }

    private onError = (error: Error) => {
        this.logger.error(error);
    }
}

export { Server };
