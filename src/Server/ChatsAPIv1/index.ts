import * as assert from 'assert';

import { Socket } from 'socket.io';
import { MessageId, v1 } from 'rp-chat-types';
import * as _ from 'lodash';

import { ChannelsService, ChatsService, MessagesService, ParticipantsSocketsService } from '@src/Service';
import { LoggerWithContext } from '@src/Logger/LoggerWithContext';

class ChatsAPIv1 {
    constructor(
        private readonly logger: LoggerWithContext,
        private readonly participantsSocketsService: ParticipantsSocketsService,
        private readonly chatsService: ChatsService,
        private readonly channelsService: ChannelsService,
        private readonly messagesService: MessagesService
    ) { }

    async listen(socket: Socket) {
        const { token, user_id } = socket.handshake.query;

        // TODO: validate user token and response with an error if it's invalid

        this.logger.debug(`Listen new socket: ${socket.id} ${token} ${user_id} ${token === 'null'}`);

        const userId = Number(user_id);
        assert(_.isString(token) && !_.isEmpty(token), `Invalid token: ${token}`);
        assert(Number.isSafeInteger(userId), `Invalid user id: ${user_id}`);

        socket.on('chats', this.onChats.bind(this, socket, userId));
        socket.on('chats/new', this.onChatsNew.bind(this, socket, userId));
        socket.on('chats/describe', this.onChatsDescribe.bind(this, socket));
        socket.on('chats/members', this.onChatsMembers.bind(this, socket, userId));
        socket.on('chats/members/edit', this.onChatsMembersEdit.bind(this, socket, userId));
        socket.on('chats/edit', this.onChatsEdit.bind(this, socket, userId));

        socket.on('messages', this.onMessages.bind(this, socket, userId));
        socket.on('messages/new', this.onNewMessage.bind(this, socket, userId));

        socket.on('channels/new', this.onChannelsNew.bind(this, socket, userId));
        socket.on('channels/describe', this.onChannelsDescribe.bind(this, socket, userId));
        socket.on('channels/read', this.onChannelsRead.bind(this, socket, userId));
        socket.on('channels/unread', this.onChannelsUnread.bind(this, socket, userId));
        socket.on('channels/members', this.onChannelsMembers.bind(this, socket));
        socket.on('channels/members/edit', this.onChannelsMembersEdit.bind(this, socket, userId));
        socket.on('channels/delete', this.onChannelsDelete.bind(this, socket, userId));
        socket.on('channels/leave', this.onChannelsLeave.bind(this, socket, userId));
        socket.on('channels/edit', this.onChannelsEdit.bind(this, socket, userId));

        socket.on('error', (error: Error) => {
            this.logger.error(`Error on socket ${socket.id}: ${error.message}`, error);
        });

        socket.on('disconnect', (reason: string) => {
            // TODO: https://socket.io/docs/v3/server-api/index.html#Event-%E2%80%98disconnect%E2%80%99
            this.logger.error(`Socket ${socket.id} disconnect error: ${reason}`);

            socket.removeAllListeners();
        });

        socket.emit('initialized');

        await this.init(socket, userId);
    }

    async stopListen(socket: Socket) {
        const { user_id } = socket.handshake.query;

        const userId = Number(user_id);
        assert(Number.isSafeInteger(userId), `Invalid user id: ${user_id}`);

        this.logger.debug(`Going to remove socket from memory for ${userId} ${socket.id}`);

        this.participantsSocketsService.delete(userId, socket);
    }

    private async init(socket: Socket, userId: number) {
        const logger = this.logger.create('[init]');

        logger.debug(`init: ${userId}`);

        try {
            const chats = await this.chatsService.allChatsIds(userId);
            const channels = await this.channelsService.allChannelsIds(userId);

            logger.debug(`chats: ${chats}, channels: ${channels}`);

            chats.forEach(_ => socket.join(_ + ''));
            channels.forEach(_ => socket.join(_ + ''));

            this.participantsSocketsService.set(userId, socket);
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChats(socket: Socket, userId: number, { messageId }: { messageId: MessageId }) {
        this.logger.debug(`onChats: ${socket.id} ${userId}`);

        try {
            const chats = await this.chatsService.get(userId);

            socket.emit('chats', chats, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChatsNew(
        socket: Socket,
        userId: number,
        chat: v1.INewChat,
        { messageId }: { messageId: MessageId }
    ) {
        const logger = this.logger.create(`[onChatsNew]({ userId: ${userId}, chat: ${JSON.stringify(chat)} })`);
        try {
            logger.debug('Start creating new chat..');

            const newChat = await this.chatsService.new(userId, chat);

            const sameUserSockets = this.participantsSocketsService.get(userId);
            const chatParticipantsSockets = _.flatMap([
                socket,
                ...chat.participants.map(_ => this.participantsSocketsService.get(_)),
                ...sameUserSockets
            ]);

            const defaultChannel = newChat.channels[0];
            assert(
                defaultChannel,
                `Invalid default channel for chat: ${userId} ${newChat.id} ${newChat.title} ${JSON.stringify(
                    newChat.channels
                )}`
            );

            chatParticipantsSockets.forEach(_ => _!.join(newChat.id + ''));
            chatParticipantsSockets.forEach(_ => _!.join(defaultChannel.id + ''));

            this.broadcast(socket, defaultChannel.id, 'chats/new', newChat, { messageId });

            logger.debug('Created new chat');
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChatsDescribe(socket: Socket, chatId: number, { messageId }: { messageId: MessageId }) {
        try {
            const chat = await this.chatsService.describe(chatId);

            socket.emit('chats/describe', chat, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChatsMembers(
        socket: Socket,
        _userId: number,
        chatId: number,
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const members = await this.chatsService.members(chatId);
            if (!members) {
                return;
            }

            socket.emit('chats/members', members, chatId, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChatsMembersEdit(
        socket: Socket,
        userId: number,
        chatId: number,
        membersToAdd: number[],
        membersToRemove: number[],
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const res = await this.chatsService.addMembers(userId, chatId, membersToAdd, membersToRemove);
            if (!res) {
                return;
            }

            const { chat, participants } = res;

            const chatJoinParticipantsSockets = _.flatMap([
                ...membersToAdd.map(_ => this.participantsSocketsService.get(_))
            ]);

            const chatLeftParticipantsSockets = _.flatMap([
                ...membersToRemove.map(_ => this.participantsSocketsService.get(_))
            ]);

            await Promise.all(chatJoinParticipantsSockets.map(_ => _!.join(chatId + '')));

            this.broadcastToSockets(chatJoinParticipantsSockets, 'chats/new', chat, { messageId: null });
            this.broadcastToSockets(chatLeftParticipantsSockets, 'chats/delete', chat, { messageId: null });
            this.broadcast(socket, chatId, 'chats/members', participants, chatId, { messageId });

            await Promise.all(chatLeftParticipantsSockets.map(_ => _!.leave(chatId + '')));
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChatsEdit(
        socket: Socket,
        userId: number,
        chat: v1.IEditChat,
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const editedChat = await this.chatsService.editChat(userId, chat.id, chat.title);
            if (!editedChat) {
                return;
            }

            this.broadcast(socket, editedChat.id, 'chats/edit', editedChat, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsNew(
        socket: Socket,
        userId: number,
        channel: v1.INewChannel,
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const createdChannel = await this.channelsService.new(userId, channel);

            const sameUserSockets = this.participantsSocketsService.get(userId);
            const channelParticipantsSockets = _.flatMap([
                socket,
                ...channel.participants.map(_ => this.participantsSocketsService.get(_)),
                ...sameUserSockets
            ]);

            channelParticipantsSockets.forEach(_ => _!.join(createdChannel.id + ''));
            this.broadcast(socket, createdChannel.id, 'channels/new', createdChannel, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsDescribe(
        socket: Socket,
        userId: number,
        channelId: number,
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const channel = await this.channelsService.describe(userId, channelId);
            if (!channel) {
                return;
            }

            socket.emit('channels/describe', channel, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsRead(
        socket: Socket,
        userId: number,
        request: v1.IReadChannel,
        { messageId }: { messageId: MessageId }
    ) {
        const logger = this.logger.create(`[onChannelsRead]({ request: ${JSON.stringify(request)} })`);

        try {
            logger.debug('Start reading channel..');

            await this.channelsService.readChannel(userId, request.channelId);

            const participants = this.participantsSocketsService.get(userId);

            logger.debug(`Read channel for ${JSON.stringify(participants.map(_ => _.id))}`);

            this.broadcastToSockets(participants, 'channels/read', request, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsUnread(socket: Socket, userId: number, { messageId }: { messageId: MessageId }) {
        try {
            const unreadChannels: v1.IUnreadChannel[] = await this.channelsService.getUnreadChannels(userId);

            socket.emit('channels/unread', unreadChannels, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsMembers(socket: Socket, channelId: number, { messageId }: { messageId: MessageId }) {
        try {
            const members = await this.channelsService.members(channelId);

            socket.emit('channels/members', members, channelId, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsMembersEdit(
        socket: Socket,
        userId: number,
        channelId: number,
        membersToAdd: number[],
        membersToRemove: number[],
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const res = await this.channelsService.addMembers(userId, channelId, membersToAdd, membersToRemove);
            if (!res) {
                return;
            }

            const { channel, participants } = res;

            const channelJoinParticipantsSockets = _.flatMap([
                ...membersToAdd.map(_ => this.participantsSocketsService.get(_))
            ]);

            const channelLeftParticipantsSockets = _.flatMap([
                ...membersToRemove.map(_ => this.participantsSocketsService.get(_))
            ]);

            await Promise.all(channelJoinParticipantsSockets.map(_ => _!.join(channelId + '')));

            this.broadcastToSockets(channelJoinParticipantsSockets, 'channels/new', channel, { messageId: null });
            this.broadcastToSockets(channelLeftParticipantsSockets, 'channels/delete', channel, { messageId: null });
            this.broadcast(socket, channelId, 'channels/members', participants, channelId, { messageId });

            await Promise.all(channelLeftParticipantsSockets.map(_ => _!.leave(channelId + '')));
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsDelete(
        socket: Socket,
        userId: number,
        request: { id: number },
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const deletedChannel = await this.channelsService.deleteChannel(userId, request.id);
            if (!deletedChannel) {
                return;
            }

            this.broadcast(socket, request.id, 'channels/delete', deletedChannel, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsLeave(
        socket: Socket,
        userId: number,
        request: { id: number },
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const leftChannel = await this.channelsService.leaveChannel(userId, request.id);
            if (!leftChannel) {
                return;
            }

            this.broadcast(socket, request.id, 'channels/leave', leftChannel, userId, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onChannelsEdit(
        socket: Socket,
        userId: number,
        channel: v1.IEditChannel,
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const editedChannel = await this.channelsService.editChannel(userId, channel.id, channel.title);
            if (!editedChannel) {
                return;
            }

            this.broadcast(socket, channel.id, 'channels/edit', editedChannel, userId, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onMessages(
        socket: Socket,
        userId: number,
        request: { channelId: number; page: number },
        { messageId }: { messageId: MessageId }
    ) {
        try {
            const { messages, page } = await this.messagesService.get(userId, request.channelId, request.page);

            socket.emit('messages', messages, page, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private async onNewMessage(
        socket: Socket,
        userId: number,
        message: v1.INewMessage,
        { messageId }: { messageId: MessageId }
    ) {
        try {
            this.logger.debug(`New message ${socket.id}, message: ${JSON.stringify(message)}`);

            const newMessage = await this.messagesService.new(userId, message);

            this.broadcast(socket, message.channelId, 'messages/new', newMessage, { messageId });
        } catch (error) {
            socket.emit('internal-error', this.internalError(error));
        }
    }

    private broadcast(socket: Socket, channelId: number, event: string, ...params: any[]) {
        socket.emit(event, ...params);
        socket.broadcast.to(channelId + '').emit(event, ...params);
    }

    // TODO: why do we need that here ?
    private broadcastToSockets(sockets: Socket[], event: string, ...params: any[]) {
        sockets.forEach(_ => _.emit(event, ...params));
    }

    private internalError(error: Error) {
        const id = Date.now();
        const message = `Internal error ${id}`;

        this.logger.error(message, error);

        return message;
    }
}

export { ChatsAPIv1 };
